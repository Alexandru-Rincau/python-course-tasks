# Программирование на языке высокого уровня (Python).
# https://www.yuripetrov.ru/edu/python
# Задание task_13_02_03.
#
# Выполнил: Фамилия И.О.
# Группа: !!!
# E-mail: !!!


from job_monitor import JobMonitor

if __name__ == "__main__":
    monitor = JobMonitor()
    monitor.run(timeout=30)
