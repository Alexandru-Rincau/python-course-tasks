# Программирование на языке высокого уровня (Python).
# https://www.yuripetrov.ru/edu/python
# Задание task_11_02_02.
#
# Выполнил: Фамилия И.О.
# Группа: !!!
# E-mail: !!!


import random
import functools
from card import Card


class CardList:
    """Класс CardList представляет набор карт
    (например, в руке у игрока или на столе).

    Поля экземпляра класса:
      - self._cards (list из Card): карты в наборе.

    Методы экземпляра класса:
      - self.shuffle(): перемешивает карты в наборе;
      - self.append(): добавляет карту в набор;
      - self.remove(): удаляет карту из набора;
      - self.pop(): удаляет и возвращает карту из набора;
      - self.is_empty(): True, если набор пустой;
      - self.sum(): считает сумму значения карт.
    """

    def __init__(self):
        """Инициализация класса."""
        raise NotImplementedError
        # Уберите raise и допишите код

    def __str__(self):
        """Вернуть строковое представление набора карт.

        Формат: 'карта1 карта2...', например
                '1 2 5', если все карты лежат лицом вверх.

        Для формирования строки используйте functools.reduce().
        """
        raise NotImplementedError
        # Уберите raise и допишите код

    def __len__(self):
        """Размер набора карт."""
        raise NotImplementedError
        # Уберите raise и допишите код

    def shuffle(self):
        """Перемешать набор карт."""
        raise NotImplementedError
        # Уберите raise и допишите код

    def append(self, card):
        """Добавить карту 'card' в набор.

        Необходимо удостовериться, что 'card' -
        экземпляр класса Card (assert).
        """
        raise NotImplementedError
        # Уберите raise и допишите код

    def pop(self, index):
        """Вытащить (удалить и вернуть) карту
        под индексом 'index' из набора."""
        raise NotImplementedError
        # Уберите raise и допишите код

    def sum(self):
        """Вернуть сумму выбранных карт."""
        raise NotImplementedError
        # Уберите raise и допишите код

    def is_empty(self):
        """Вернуть True, если в наборе нет карт."""
        raise NotImplementedError
        # Уберите raise и допишите код
